EXCEPTIONS = ['quelle','quelque','encore','contre','devant','toujours','faisait',
              'toutes','étaient','lautre','allait','autour','cétait','avaient',
              'tandis','grands','chaque','jamais','depuis','pourtant','bonjour',
              'Cétait']

def lire(fichier):
    '''
    lit un fichier .txt et renvoie le texte contenu sous forme d'une chaine de caratères
    '''
    pass

def lister_tous_mots(fichier):
    '''
    on retire du texte les symboles indésirables et enfin
    grace à la méthode split on découpe le texte et on renvoie la liste des mots
    '''
    pass

def lister_mots_importants(fichier,exceptions = EXCEPTIONS):
    '''
    on renvoie la liste des mots présents dans le fichier qui ne sont pas dans exceptions
    '''
    pass

def occurences_mots(fichier,lettres_min=5,exceptions =EXCEPTIONS):
    '''
    on crée et renvoie un dictionnaire contenant les mots d'au moins 5 lettres
    et qui ne sont pas présent dans les exceptions
    auxquels on associe le nombre de fois où le mot apparait dans le texte
    '''
    pass

def nombre_mots(fichier):
    '''
    renvoie le nombre de mots dans le fichier
    '''
    pass

def nature_texte(fichier):
    '''
    renvoie la nature du texte (poésie ou fable, pièce de théatre ou nouvelle, roman)
    suivant le nombre de mots dans le fichier
    '''
    pass

def liste_mots_ranges(fichier,nombre = 1000, lettres_min=5,exceptions =EXCEPTIONS):
    '''
    crée une liste de tuples provenant du dictionnaire comme précédemment sauf
    que celui ci est trié par ordre décroissant d'apparitions
    '''
    pass

def liste_noms(fichier,nombre = 100, lettres_min=5,exceptions =EXCEPTIONS):
    '''
    renvoie la liste des noms qui apparaissent le plus dans le fichier
    on considère les mots qui commencent par une majuscule
    '''
    pass

def liste_mots(fichier,nombre = 100, lettres_min=5,exceptions =EXCEPTIONS):
    '''
    renvoie la liste des mots qui apparaissent le plus dans le fichier
    on considère les mots qui commencent par une minuscule
    '''
    pass
        