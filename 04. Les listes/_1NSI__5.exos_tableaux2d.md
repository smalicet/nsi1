# Exercice n°1 : QCM Tableaux à 2 dimensions  &#x1F3C6; 

__Question 1:__   Soit la liste suivante : `t = [[12, 13, 14], [6, 16, 18], [9, 14, 17]]`.      
Comment accéder à la donnée `14` ?   

* [ ] `t[1][3]`
* [ ] `t[0][3]`
* [ ] `t[0][2]`
* [ ] `t[14]`

__Question 2:__  Comment créer une liste qui contient 4 listes de 6 éléments?      

* [ ]  `[0 for j in range(6) for i in range(4)]`
* [ ]  `[[0 for j in range(4)] for i in range(6)]`
* [ ]  `[[0 for j in range(6)] for i in range(4)]`
* [ ]  `[[0 for j in range(4)] for i in range(4)]`   

__Question 3:__  (_Extrait de qcm sujet 0 prime_)

Quelle est la valeur de la variable `image` après exécution du programme Python suivant ?        

```python
image = [[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0],[0, 0, 0, 0]]
for i in range(4):
   for j in range(4):
      if (i+j) == 3:
         image[i][j] = 1
```

* [ ]  `[[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]`
* [ ]  `[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]`
* [ ]  `[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]`
* [ ]  `[[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 1, 1, 1]]`  

# Exercice n°2: Vérifier un carré magique &#x1F3C6;&#x1F3C6;
(_Extrait de prépabac NSI, Hatier_)

![carré magique](../fig/carre_magique.jpg)

Un carré d'ordre n est un tableau carré contenant $`n^{2}`$ entiers strictement positifs.  
On dit qu'un carré d'ordre _n_ est magique si :  

* il contient tous les nombres entiers de 1 à $`n^{2}`$;
* les sommes des nombres de chaque rangée, les sommes des nombres de chaque colonne et les sommes des nombres de chaque diagonale principale sont égales.  

On modélise un carré par une liste de liste de nombres.

__1)__   
__a-__  Quelle est la valeur de `len(carre4)`?   
__b-__ Quelle est la valeur de `carre3[1]`?    
__c-__ Quelle est la valeur de `carre3[0][2]`?    
__d-__ Quelle instruction permet de récupérer la valeur `3` de `carre4` ?    

__2) a-__  On propose le code suivant:

```python
def somme_ligne(carre, n):
   """
   carre est un tableau carré de nombres
   n est un nombre entier
   """
   somme = 0
   for nombre in carre[n]:
      somme = somme + nombre
   return somme
```

Que vaut `somme_ligne(carre4, 2)` ?
A quoi sert cette fonction ?

__b-__ Proposer le code d'une fonction qui prend un carré en paramètre, ainsi que le numéro d'une colonne, et qui renvoie la somme des nombres de cette colonne.   
__c-__ 🥇 Ecrire le code d'une fonction qui prend un carré en paramètre et qui vérifie que les sommes des nombres de chaque ligne sont égales.  
__d-__ 🥇🥇 _POUR ALLER PLUS LOIN :_ Définissez un prédicat `est_magique()` qui prend un carré en paramètre et qui renvoie un booléen indiquant si le carré est bien magique.  

# Exercice n°3 : Création de listes de listes en compréhension  &#x1F3C6; &#x1F3C6; &#x1F3C6;  

On peut réaliser plusieurs tests dans les listes par compréhension, par exemple :

```python
>>> t = ['*' if i%2==0 else i for i in range(11)]
>>> t
 ['*', 1, '*', 3, '*', 5, '*', 7, '*', 9, '*']
```

la liste t est une liste des entiers compris entre  0 et 10 inclus affichant :  

* des symboles `*` si les entiers sont pairs
* les entiers dans le cas contraire  

Etudiez bien la structure employée et réutilisez-la dans les exemples qui suivent.

__1)__ Créer une liste par compréhension à deux dimensions 3x3 qui comportent un 0 si la somme de l'indice de ligne et l'indice de colonne est paire sinon un 1.   

```python
[[0, 1, 0], [1, 0, 1], [0, 1, 0]]
```

\newpage

__2)__ Créer une liste par compréhension qui a pour affichage :    

```
1 0 0 0 0 0 0 0 0 0  
0 1 0 0 0 0 0 0 0 0  
0 0 1 0 0 0 0 0 0 0  
0 0 0 1 0 0 0 0 0 0  
0 0 0 0 1 0 0 0 0 0  
0 0 0 0 0 1 0 0 0 0  
0 0 0 0 0 0 1 0 0 0  
0 0 0 0 0 0 0 1 0 0  
0 0 0 0 0 0 0 0 1 0  
0 0 0 0 0 0 0 0 0 1
```

__On créera une fonction pour créer la liste et on utilisera le cours pour créer une fonction qui affichera cette liste.__

__3)__  Créer une liste par compréhension qui a pour affichage :  

```
1 0 0 0 0 0 0 0 0 1  
0 1 0 0 0 0 0 0 1 0  
0 0 1 0 0 0 0 1 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 0 1 1 0 0 0 0  
0 0 0 1 0 0 1 0 0 0  
0 0 1 0 0 0 0 1 0 0  
0 1 0 0 0 0 0 0 1 0  
1 0 0 0 0 0 0 0 0 1  
```

__4)__ Créer une liste par compréhension qui a pour affichage :  

```
1 0 0 0 0 0 0 0 0 0  
1 1 0 0 0 0 0 0 0 0  
1 1 1 0 0 0 0 0 0 0  
1 1 1 1 0 0 0 0 0 0  
1 1 1 1 1 0 0 0 0 0  
1 1 1 1 1 1 0 0 0 0  
1 1 1 1 1 1 1 0 0 0  
1 1 1 1 1 1 1 1 0 0  
1 1 1 1 1 1 1 1 1 0  
1 1 1 1 1 1 1 1 1 1  
```
