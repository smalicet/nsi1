alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def conversion(n,b):
    reponse = ''
    if n == 0:
        return '0'
    else:
        while  n > 0:
           r = n % b
           n = n // b
           reponse = alphabet[r] + reponse
        return reponse
    
def deconversion(n,b):
    puissance = 0
    reponse = 0
    for i in range(len(n)-1,-1,-1):
        reponse = reponse + alphabet.index(n[i])*b**puissance
        puissance = puissance + 1
    return reponse
