from microbit import *
import radio

radio.config(channel=12)
radio.on()

JoyStick_X = pin1
JoyStick_Y = pin2
KEY_A = pin5
KEY_B = pin11
KEY_C = pin13
KEY_D = pin14
KEY_L = pin15
KEY_R = pin16
DIR = {
    'NONE': 0,
    'U': 1,
    'D': 2,
    'L': 3,
    'R': 4,
    'U_L': 5,
    'U_R': 6,
    'D_L': 7,
    'D_R': 8
}
KEY = {
    'NONE': 0,
    'A': 2,
    'B': 3,
    'C': 4,
    'D': 5,
    'L': 6,
    'R': 7
}

class JOYSTICK():
    def __init__(self):
        self.Read_X = JoyStick_X.read_analog()
        self.Read_Y = JoyStick_Y.read_analog()
        pin13.set_pull(pin13.PULL_UP)
        pin14.set_pull(pin14.PULL_UP)
        pin15.set_pull(pin15.PULL_UP)
        pin16.set_pull(pin16.PULL_UP)

    def Listen_Dir(self, Dir):
        Get_Rocker = DIR['NONE']
        New_X = JoyStick_X.read_analog()
        New_Y = JoyStick_Y.read_analog()

        Dx = abs(self.Read_X - New_X)
        Dy = abs(self.Read_Y - New_Y)

        Right = New_X - self.Read_X
        Left = self.Read_X - New_X
        Up = New_Y - self.Read_Y
        Down = self.Read_Y - New_Y

        # max = 1023
        Precision = 150

        if Right > Precision and Dy < Precision:
            Get_Rocker = DIR['R']
        elif Left > Precision and Dy < Precision:
            Get_Rocker = DIR['L']
        elif Up > Precision and Dx < Precision:
            Get_Rocker = DIR['U']
        elif Down > Precision and Dx < Precision:
            Get_Rocker = DIR['D']
        elif Right > Precision and Up > Precision:
            Get_Rocker = DIR['U_R']
        elif Right > Precision and Down > Precision:
            Get_Rocker = DIR['D_R']
        elif Left > Precision and Up > Precision:
            Get_Rocker = DIR['U_L']
        elif Left > Precision and Down > Precision:
            Get_Rocker = DIR['D_L']
        else:
            Get_Rocker = DIR['NONE']

        if Dir == Get_Rocker:
            return True
        else:
            return False

    def Listen_Key(self, Key):
        read_key = KEY['NONE']
        if KEY_A.read_digital() == 0:
            read_key = KEY['A']
        elif KEY_B.read_digital() == 0:
            read_key = KEY['B']
        elif KEY_C.read_digital() == 0:
            read_key = KEY['C']
        elif KEY_D.read_digital() == 0:
            read_key = KEY['D']
        elif KEY_L.read_digital() == 0:
            read_key = KEY['L']
        elif KEY_R.read_digital() == 0:
            read_key = KEY['R']
        else:
            read_key = KEY['NONE']

        if Key == read_key:
            return True
        else:
            return False

    def message(self):
        while self.Listen_Dir(DIR['U']):
            radio.send('U')
        while self.Listen_Dir(DIR['D']):
            radio.send('D')
        while self.Listen_Dir(DIR['L']):
            radio.send('L')
        while self.Listen_Dir(DIR['R']):
            radio.send('R')
        while self.Listen_Dir(DIR['U_L']):
            radio.send('U_L')
        while self.Listen_Dir(DIR['U_R']):
            radio.send('U_R')
        while self.Listen_Dir(DIR['D_L']):
            radio.send('D_L')
        while self.Listen_Dir(DIR['D_R']):
            radio.send('D_R')
        while self.Listen_Key(KEY['A']):
            radio.send('A')
        while self.Listen_Key(KEY['B']):
            radio.send('B')
        while self.Listen_Key(KEY['C']):
            radio.send("C")
        while self.Listen_Key(KEY['D']):
            radio.send("D")
        while self.Listen_Key(KEY['L']):
            radio.send("L")
        while self.Listen_Key(KEY['R']):
            radio.send("R")
        display.clear()


JoyStick = JOYSTICK()

while True:
    JoyStick.message()
