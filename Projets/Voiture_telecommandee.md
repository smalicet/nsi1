# La Voiture Télécommandée

### Objectif : 
### Programmer en python microbit, la voiture ainsi que la télécommande de façon à faire fonctionner la voiture.



# 


La programmation se fait en micro python, accessible grace au logiciel Mu qui est installé sur les PC.

Pour rappel, voici le [TD de seconde sur la programmation des cartes microbits](https://framagit.org/smalicet/nsi1/-/blob/main/Projets/_TNSI__TP._Microbit-prof.pdf).

Vous trouverez également toute la [documentation ici](https://microbit-micropython.readthedocs.io/fr/latest/)

Voici les réponses aux questions que vous vous poserez forcément :

1. *Comment faire communiquer les deux appareils ?*   
Il faut comprendre comment on utilise le module radio en micro python.  

2. *Comment faire avancer la voiture ?*  
Il faut regarder de près la voiture et remarquer quels sont les pins à activer pour actionner les moteurs.   
On utilisera alors la syntaxe pin **???** .write_analog( **???** ) pour actionner le moteur.

3. *Comment utiliser la télécommande ?*   
Puisque c'est compliqué, voici le [programme déjà fait ici](https://framagit.org/smalicet/nsi1/-/blob/main/Projets/bitplayer.py).    
A vous toutefois de comprendre comment il fonctionne.

Bon courage à vous !
