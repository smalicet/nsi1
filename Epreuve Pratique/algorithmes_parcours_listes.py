# CALCUL DE LA SOMME DES ELEMENTS D UNE LISTE

# Exemple :
# >>> somme([2,4,6])
# 12

def somme(liste):
    s = 0                #on initialise la somme à 0
    for nombre in liste: #pour chaque nombre de la liste
        s = s + nombre   #la somme augmente du nombre
    return s             #on renvoie la somme obtenue

def somme(liste):
    s = 0
    for i in range(len(liste)):   #ici on parcours les indices
        s = s + liste[i]          #on ajoute le i ème nombre de la liste
    return s




# CALCUL DE LA MOYENNE D UNE LISTE

# Exemple :
# >>> moyenne([2,4,6])
# 4

def moyenne(liste):
    s = 0                #on initialise la somme à 0
    for nombre in liste: #pour chaque nombre de la liste
        s = s + nombre   #la somme augmente du nombre
    m = s / len(liste)   #la moyenne est égale à la somme divisée par le nombre de notes
    return m             #on renvoie la moyenne




# CALCUL DU MINIMUM D UNE LISTE

# Exemple :
# >>> minimum([2,4,6])
# 2

def minimum(liste):
    mini = liste[0]     #on initialise le minimum au premier élement de la liste
    for nombre in liste:   #pour chaque nombre de la liste
        if nombre < mini:  #si le nombre est plus petit que le minimum
            mini = nombre  #alors il remplace le minimum
    return mini         #on renvoie le minimum obtenu après tout le parcours de la liste

def minimum(liste):
    mini = liste[0]     #on initialise le minimum au premier élement de la liste
    for i in range(len(liste)):   #pour chaque inidice de la liste
        if liste[i] < mini:  #si le nombre (le i ème élement de la liste) est plus petit que le minimum
            mini = liste[i]  #alors il remplace le minimum
    return mini         #on renvoie le minimum obtenu après tout le parcours de la liste



# CALCUL DU MAXIMUM D UNE LISTE

# Exemple :
# >>> maximum([2,4,6])
# 6

def maximum(liste):
    maxi = liste[0]     #on initialise le minimum au premier élement de la liste
    for nombre in liste:   #pour chaque nombre de la liste
        if nombre > maxi:  #si le nombre est plus petit que le minimum
            maxi = nombre  #alors il remplace le minimum
    return maxi         #on renvoie le minimum obtenu après tout le parcours de la liste

def maximum(liste):
    maxi = liste[0]     #on initialise le minimum au premier élement de la liste
    for i in range(len(liste)):   #pour chaque inidice de la liste
        if liste[i] > maxi:  #si le nombre (le i ème élement de la liste) est plus petit que le minimum
            maxi = liste[i]  #alors il remplace le minimum
    return maxi         #on renvoie le minimum obtenu après tout le parcours de la liste




# CALCUL DU NOMBRE D OCCURENCES (DE REPETITION) D UN ELEMENT DANS UNE LISTE

# Exemple :
# >>> nb_occurences(6,[2,4,6])
# 1

def nb_occurences(valeur, liste):
    n = 0             #on initialise le nombre d'occurences à 0
    for element in liste:        #on parcours chaque element de la liste
        if element == valeur:    #si l element correspond à la valeur chercheee
            n = n + 1            #alors le nombre d'occurence augmente de 1
    return n           #on renvoie le nombre d'occurences obtenu après avoir parcouru tous les elements

def nb_occurences(valeur, liste):
    n = 0             #on initialise le nombre d'occurences à 0
    for i in range(len(liste)):        #on parcours chaque indice de la liste
        if liste[i] == valeur:    #si le i ème element de la liste correspond à la valeur chercheee
            n = n + 1            #alors le nombre d'occurence augmente de 1
    return n           #on renvoie le nombre d'occurences obtenu après avoir parcouru tous les elements




# RECHERCHE DES INDICES D UN ELEMENT DANS UNE LISTE

# Exemple :
# >>> indices(6,[2,4,6])
# [2]

def indices(valeur, liste):
    liste_indice = []                 #on initialise la liste des indices trouvés (au début une liste vide)
    for i in range(len(liste)):       #on parcourt chaque indice de la liste
        if liste[i] == valeur:        #si le i ème element correspond à la valeur cherchée
            liste_indice.append(i)    #alors on ajoute l'indice i à la liste des indices
    return liste_indice               #on renvoie la liste des indices obtenus après le parcours de toute la liste
             
             
             

# VERIFIE SI UNE LISTE EST TRIEE DANS L ORDRE CROISSANT

# Exemple :
# >>> verifie(6,[2,4,6])
# True

def verifie(liste):
    for i in range(len(liste)-1):   #pour chaque indice de la liste jusqu'à l'avant dernier
        if liste[i] > liste[i+1]:   #si le i ème élément est plus grand que le suivant
            return False            #alors on renvoie False car la liste n'est pas triée
    return True                     #si on arrive à la fin du parcours sans avoir d'erreurs de tri(False) alors on renvoie True(la liste est triée)





# INVERSE LES ELEMENTS D UNE LISTE

# Exemple :
# >>> inverse([2,8,9,1])
# [1,9,8,2]

def inverse(liste):
    l = []
    for element in liste:
        l = list(element) + liste
    return l


