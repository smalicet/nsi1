with open("ladisparition.txt","r",encoding="utf-8") as fichier:
    txt = fichier.read()

def nombre_occurence_mot(mot,texte):
    liste_mots = texte.split()
    compteur = 0
    for element in liste_mots:
        if element.lower() == mot or element.lower() == mot+'.' or element.lower() == mot+',' or element.lower() == mot+';' or element.lower() == mot+'!' or element.lower() == mot+'?':
            compteur = compteur + 1
    return compteur

def nombre_occurence_mot2(mot,texte):
    compteur = 0
    n = len(texte)
    m = len(mot)
    for i in range(len(texte)-m):
        j = 0
        while j<m and mot[j] == texte[i+j].lower():
            j = j + 1
        if j == m:
            compteur = compteur + 1
    return compteur
        
print(nombre_occurence_mot("alors",txt))
print(nombre_occurence_mot2("alors",txt))
